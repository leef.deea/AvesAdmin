export const environment = {
  production: true,
  envName: 'PROD',
  apiBaseUrl: 'http://localhost:8080/aves/api/'
};
