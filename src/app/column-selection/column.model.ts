export class ColumnModel {

  column_name: string;
  column_abbr: string;
  spreadsheet: string;

  constructor( name, abbr, spreadsheet ) {
    this.column_name = name;
    this.column_abbr = abbr;
    this.spreadsheet = spreadsheet;
  }
}

export class InitColModel {

  getColumns(): ColumnModel[] {

    const columns: ColumnModel[] = [];

    columns.push( new ColumnModel('Project', 'prj', 'Detail') );
    columns.push( new ColumnModel('Gov POC', 'gpoc', 'Detail') );
    columns.push( new ColumnModel('Application', 'app', 'Detail') );
    columns.push( new ColumnModel('StartDt', 'std', 'Detail') );
    columns.push( new ColumnModel('Date', 'ddt', 'Sprint') );
    columns.push( new ColumnModel('Iteration', 'prj', 'Sprint') );
    columns.push( new ColumnModel('Velocity', 'prj', 'Sprint') );
    columns.push( new ColumnModel('Agile', 'agg', 'Sprint') );
    columns.push( new ColumnModel('Developers', 'dev', 'Sprint') );
    columns.push( new ColumnModel('Project Scope', 'dev', 'Quality') );
    columns.push( new ColumnModel('TEMs', 'dev', 'Quality') );
    columns.push( new ColumnModel('Notional Schedule', 'dev', 'Quality') );
    columns.push( new ColumnModel('Budget', 'dev', 'Quality') );
    columns.push( new ColumnModel('FRD\'s', 'dev', 'Quality') );
    columns.push( new ColumnModel('Roadmap', 'dev', 'Quality') );
    columns.push( new ColumnModel('CIO Approval', 'dev', 'Quality') );

    return columns;

  }
}
