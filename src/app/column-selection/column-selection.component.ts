import { Component, OnInit } from '@angular/core';
import { InitColModel, ColumnModel } from './column.model';
import { Column } from 'ag-grid';

@Component({
  selector: 'app-column-selection',
  templateUrl: './column-selection.component.html',
  styleUrls: ['./column-selection.component.css']
})
export class ColumnSelectionComponent implements OnInit {

  Spreadsheet: string[] = [];
  GroupedColumn = {};
  cols = [];
  constructor() {

    const columns: ColumnModel[] = new InitColModel().getColumns();
    for ( let i = 0; i < 50; i++ ) {
      this.cols.push(i);
    }

    for ( const col of columns ) {
      if ( !this.GroupedColumn[col.spreadsheet] ) {
        this.GroupedColumn[col.spreadsheet] = [];
        this.Spreadsheet.push(col.spreadsheet);
      }
      this.GroupedColumn[col.spreadsheet].push(col);
    }

    console.log( this.GroupedColumn );
  }

  ngOnInit() {
  }

}
