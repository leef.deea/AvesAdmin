import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store, Actions, ofActionSuccessful, ofActionDispatched, ofActionErrored, Select } from '@ngxs/store';
import { ActivatedRoute, Router, CanDeactivate } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Company } from '../store/company.model';
import { LoadCompany, LoadCompanySuccess, ActionFailed, UpdateCompany, AddCompany } from '../store/company.action';
import { Observable, Subscription } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthState } from '../../login/auth.state';
import { CanDeactivateGuard } from '../../can-deactivate-guard';
import { Modal } from 'ngx-modialog/plugins/bootstrap';

@Component({
  selector: 'app-company-edit',
  templateUrl: './company-edit.component.html',
  styleUrls: ['./company-edit.component.css']
})
export class CompanyEditComponent implements OnInit, CanDeactivateGuard {
  id: number;
  company: Company;
  companyEditForm: FormGroup;
  @Select(AuthState.isAuthorized) authorized$: Observable<string>;

  constructor(
    private store: Store,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private action: Actions,
    private modal: Modal
   ) {

  }

  ngOnInit() {


    this.companyEditForm = new FormGroup({
      name: new FormControl(null, [Validators.required, Validators.minLength(4)])
    });

    this.route.params.subscribe( ({id}) => {

      this.id = id;


      if ( id === 'new') {
        this.company = new Company(null, null, null);
        return;
      }

      this.store.dispatch( new LoadCompany(id) );

      // this.action.pipe(
      //   ofActionSuccessful(LoadCompanySuccess)
      // ).pipe( tap((data: LoadCompanySuccess) => {
      //   this.company = data.payload;
      // }) ).subscribe();


      // this.company = this.action.pipe(
      //   ofActionSuccessful(LoadCompanySuccess)
      // ).pipe( map((data: LoadCompanySuccess) => {
      //   return data.payload;
      // }) );

      // this.company.subscribe( (data) => { console.log(data); });

      this.action.pipe( ofActionSuccessful(LoadCompanySuccess) ).subscribe(
        ({payload}) => {
          this.company = payload;
          this.companyEditForm.setValue( {
            name: this.company.name
          });
        }
      );
    });
  }

  onSubmit() {
    this.company.name = this.companyEditForm.get('name').value;
    if ( this.company.id == null ) {
      this.store.dispatch( new AddCompany( this.company ));
      this.companyEditForm.reset();
      // new
    } else {
      this.store.dispatch( new UpdateCompany( this.company ));
      this.companyEditForm.reset();
    }
  }

  canDeactivate() {


    if ( this.companyEditForm.pristine ) {
      return true;
    }

    const dialogRef = this.modal.confirm()
    .title(' ')
    .headerClass('modal-header-flee')
    .footerClass('modal-footer-flee')
    .body(`Press OK to abandon changes.`)
    .open();

    return dialogRef.result;
    // .then( result => alert(`The result is: ${result}`) );

    // this.store.dispatch(new Msg('warning', 'Unsaved changes exist.'));
    // return false;
  }

}
