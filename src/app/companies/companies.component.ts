import { Component, OnInit } from '@angular/core';
import { Company } from './store/company.model';
import { Store, Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { CompanyStateModel } from './store/company.state';
import { AddCompany, LoadAllCompany, DeleteCompany } from './store/company.action';
import { AuthState } from '../login/auth.state';
import { Router } from '@angular/router';
import { Modal } from 'ngx-modialog/plugins/bootstrap';

@Component({
  selector: 'app-companies',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.css']
})
export class CompaniesComponent implements OnInit {

  selectedId: number;
  @Select(AuthState.isAuthorized) authorized$: Observable<string>;
  @Select() companies$: Observable<CompanyStateModel>;

  constructor(private store: Store,
    private router: Router,
    private modal: Modal
  ) { }

  ngOnInit() {
    const companyState = this.store.selectSnapshot( (state) => state.companies );
    if ( !companyState.loaded ) {
      this.store.dispatch( new LoadAllCompany() );
    }
  }

  add() {
    this.router.navigateByUrl('/companies/new');
  }

  edit( id: number ) {
    if (!id) {
      id = this.selectedId;
    }
    this.router.navigateByUrl('/companies/' + id);
  }

  delete( ) {


    const dialogRef = this.modal.confirm()
    .title(' ')
    .headerClass('modal-header-flee')
    .footerClass('modal-footer-flee')
    .body(`Press OK to delete Company.`)
    .open();

    dialogRef.result.then( result => {
      this.store.dispatch( new DeleteCompany(this.selectedId) );
    } );

  }

  select( id: number ) {
    this.selectedId = id;
  }
  isSelected( id: number) {
    return this.selectedId === id;
  }

}
