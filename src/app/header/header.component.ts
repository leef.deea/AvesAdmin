import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';
import { filter, tap } from 'rxjs/operators';
import { Actions, ofActionSuccessful, Store, Select } from '@ngxs/store';
import { LoginSuccess, Logout } from '../login/auth.action';
import { Observable, Subscription } from 'rxjs';
import { AuthState } from '../login/auth.state';
import { Msg, MsgStateModel, MsgState, MsgType } from './msg.state';
import { ToastrService } from 'ngx-toastr';
import { Location } from '@angular/common';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {

  msgSub: Subscription;
  @Select(AuthState.isAuthorized) authorized$: Observable<string>;
  // @Select((state) => state.messages) omsg$: Observable<MsgStateModel>;
  @Select(MsgState.selMsg) msg$: Observable<MsgStateModel>;

  environmentName = environment.envName;


  homeRoute = true;
  constructor(
    private router: Router,
    private action$: Actions,
    private store: Store,
    private toastr: ToastrService,
    private location: Location
  ) { }


  ngOnInit() {

      this.router.events.subscribe( (event: any) => {
      if ( event.routerEvent ) {
        this.homeRoute = event.routerEvent.url === '/';
      }
    });

    this.msgSub = this.msg$.pipe( tap(
      (data) => {
        switch ( data.type ) {
          case MsgType.success: this.toastr.success(data.message, 'Success');
          break;
          case MsgType.warning: this.toastr.warning(data.message, 'Warning');
          break;
          case MsgType.error: this.toastr.error(data.message, 'Error');
          break;
        }
        if ( data.path ) {
          switch ( data.path ) {
            case '$back$': this.location.back();
            break;
            default: this.router.navigateByUrl(data.path);
          }
        }
      })
    ).subscribe();

    console.log( this.msgSub );

    // this.user$.subscribe( (data) => { console.log( 'aaa' ); console.log( data ); });

    // this.loginSuccess$ = this.action$.pipe(ofActionSuccessful(LoginSuccess));
    // this.loginSuccess$.subscribe( ( data ) => console.log(data) );

    // this.action$.pipe( ofActionSuccessful(Msg)).subscribe(
    //   ( data ) => {

    //     switch ( data.type ) {
    //       case 'success': this.toastr.success(data.msg, 'Success');
    //       break;
    //       case 'warning': this.toastr.warning(data.msg, 'Warning');
    //       break;
    //       case 'error': this.toastr.error(data.msg, 'Error');
    //       break;
    //     }
    //     if ( data.path ) {
    //       switch ( data.path ) {
    //         case '$back$': this.location.back();
    //         break;
    //         default: this.router.navigateByUrl(data.path);
    //       }
    //     }
    //   }
    // );

  }

  logout() {
    this.store.dispatch( new Logout());
  }

  ngOnDestroy() {
    if ( this.msgSub ) {
      this.msgSub.unsubscribe();
    }
  }

}
