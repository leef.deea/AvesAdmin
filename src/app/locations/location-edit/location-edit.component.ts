import { Component, OnInit, OnDestroy } from "@angular/core";
import { Address } from "../store/address.model";
import { Select, Store, Actions, ofActionSuccessful } from "@ngxs/store";
import { AuthState } from "../../login/auth.state";
import { Observable, Subscription } from "rxjs";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { Modal } from "ngx-modialog/plugins/bootstrap";
import { debounceTime } from 'rxjs/operators';


import {
  LoadLocation,
  LoadLocationSuccess,
  AddLocation,
  UpdateLocation
} from "../store/location.action";
declare var google: any;

@Component({
  selector: "app-location-edit",
  templateUrl: "./location-edit.component.html",
  styleUrls: ["./location-edit.component.css"]
})
export class LocationEditComponent implements OnInit, OnDestroy {
  id: number;
  location: Address;
  locationEditForm: FormGroup;
  @Select(AuthState.isAuthorized)
  authorized$: Observable<string>;
  subs: Subscription[] = [];

  constructor(
    private store: Store,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private action: Actions,
    private modal: Modal
  ) {}

  ngOnInit() {
    this.subs.push(
      this.route.params.subscribe(({ id }) => {
        this.id = id;

        if (id === "new") {
          this.location = new Address(null, null, null);
          this.initForm();
          return;
        }

        this.store.dispatch(new LoadLocation(id));

        this.subs.push(
          this.action
            .pipe(ofActionSuccessful(LoadLocationSuccess))
            .subscribe(({ payload }) => {
              this.location = payload;
              this.initForm();
            })
        );
      })
    );
  }

  showMap(address: string) {


    const googleMapEl =  document.getElementById("googleMap");
    console.log(googleMapEl);
    if ( googleMapEl === null  ) {
      return;
    }

    const geocoder = new google.maps.Geocoder();
    geocoder.geocode({ address: address }, function(results, status) {

      if (status == google.maps.GeocoderStatus.OK) {
        const latitude = results[0].geometry.location.lat();
        const longitude = results[0].geometry.location.lng();
        const latlng = new google.maps.LatLng(latitude, longitude);
        const mapProp = {
          center: latlng,
          zoom: 14,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        const marker = new google.maps.Marker({
          position: latlng,
          title: this.location.name
        });

        const map = new google.maps.Map(
          document.getElementById("googleMap"),
          mapProp
        );
        marker.setMap(map);
      }
    });
  }

  initForm() {

    this.locationEditForm = new FormGroup({
      name: new FormControl(this.location.name, [
        Validators.required,
        Validators.minLength(4)
      ]),
      addressLine1: new FormControl(this.location.addressLine1, [
        Validators.required,
        Validators.minLength(5)
      ]),
      addressLine2: new FormControl(this.location.addressLine2),
      city: new FormControl(this.location.city, [
        Validators.required,
        Validators.minLength(4)
      ]),
      state: new FormControl(this.location.state, [
        Validators.required,
        Validators.minLength(2)
      ]),
      zipcode: new FormControl(this.location.zipcode, [
        Validators.required,
        Validators.minLength(5),
        Validators.pattern("^[0-9]{5}$")
      ])
    });

    this.subs.push(this.locationEditForm.valueChanges.pipe(debounceTime(1500)).subscribe( (change) => {

      if ( change.state || change.city || change.zipcode ) {
        const address = change.addressLine1 + ' ' +
        change.city + ', ' +
        change.state + ', ' +
        change.zipcode;
        this.showMap(address);
      }
    }));
    setTimeout(() => {
      const address = this.location.addressLine1  + ' ' +
        this.location.city + ', ' +
        this.location.state + ', ' +
        this.location.zipcode;
      this.showMap(address);
    }, 500);

  }

  onSubmit() {
    this.location.name = this.locationEditForm.get("name").value;
    this.location.addressLine1 = this.locationEditForm.get(
      "addressLine1"
    ).value;
    this.location.addressLine2 = this.locationEditForm.get(
      "addressLine2"
    ).value;
    this.location.city = this.locationEditForm.get("city").value;
    this.location.state = this.locationEditForm.get("state").value;
    this.location.zipcode = this.locationEditForm.get("zipcode").value;

    if (this.location.id == null) {
      this.store.dispatch(new AddLocation(this.location));
      this.locationEditForm.markAsPristine();
      // new
    } else {
      this.store.dispatch(new UpdateLocation(this.location));
      this.locationEditForm.markAsPristine();
    }
  }

  canDeactivate() {
    if (this.locationEditForm.pristine) {
      return true;
    }

    const dialogRef = this.modal
      .confirm()
      .title(" ")
      .headerClass("modal-header-flee")
      .footerClass("modal-footer-flee")
      .body(`Press OK to abandon changes.`)
      .open();

    return dialogRef.result;
    // .then( result => alert(`The result is: ${result}`) );

    // this.store.dispatch(new Msg('warning', 'Unsaved changes exist.'));
    // return false;
  }

  ngOnDestroy() {
    this.subs.forEach( (sub) => { sub.unsubscribe(); } );
  }
}
