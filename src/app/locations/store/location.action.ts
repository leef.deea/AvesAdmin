import { Address } from './address.model';

export class AddLocation {
  static readonly type = '[Location] Add';
  constructor( public payload: Address ) {}
}

export class UpdateLocation {
  static readonly type = '[Location] Update';
  constructor( public payload: Address ) {}
}

export class LoadLocation {
  static readonly type = '[Location] Load';
  constructor( public payload: number ) {}
}

export class LoadAllLocation {
  static readonly type = '[Location] Load All';
}

export class DeleteLocation {
  static readonly type = '[Location] Delete';
  constructor( public id: number ) {}
}

export class LoadLocationSuccess {
  static readonly type = '[Location] Load Success';
  constructor( public payload: Address ) {}
}

export class ActionLocationFailed {
  static readonly type = '[Location] Failure';
  constructor( public payload: Error ) {}
}
