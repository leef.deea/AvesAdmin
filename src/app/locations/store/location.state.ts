import { State, Action, StateContext, Store } from '@ngxs/store';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap, filter } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { AddLocation, LoadLocation, LoadLocationSuccess, LoadAllLocation, DeleteLocation, UpdateLocation } from './location.action';
import { Msg, MsgType } from '../../header/msg.state';
import { Address } from './address.model';
​

@Injectable()

export class LocationStateModel {
  loaded: boolean;
  locations: Address[];
}

@State<LocationStateModel>({
  name: 'locations',
  defaults: {
    loaded: false,
    // companies: [new Location(1, 'abc', 'flee'), new Location(2, 'xyz', 'flee')]
    locations: []
  }
})

export class LocationState {

  apiBaseUrl = environment.apiBaseUrl;

  constructor( private httpClient: HttpClient, private store: Store ) {}

  @Action(AddLocation)
  addLocation(ctx: StateContext<LocationStateModel>, action: AddLocation ) {
    const state = ctx.getState();
    this.httpClient.post(this.apiBaseUrl + 'locations', action.payload, { responseType: 'text' } ).subscribe(
      (data: string) => {
        if ( data.indexOf( 'ok:') === 0 ) {
          const newId = +data.substring(3);
          this.store.dispatch( [new LoadLocation(newId), new Msg(MsgType.success, 'Location created!', '/locations')]);
        }
      }
    );


  }


  @Action(LoadLocation)
  loadLocation(ctx: StateContext<LocationStateModel>, action: LoadLocation ) {
    const id: number = +action.payload;
    this.httpClient.get<Address>(this.apiBaseUrl + 'locations/' + id).subscribe(
      (location) => {

        const state = ctx.getState();
        // filter out existing.
        let idx = 0;
        const clone = state.locations.slice(0);

        const found = clone.some( (c) => {
          if (c.id === id) {
            clone[idx] = location;
            return true;
          }
          idx++;
        });
        if ( !found ) {
          clone.push(location);
        }

        ctx.patchState( {
          locations: clone
        });
        console.log( location );
        this.store.dispatch( new LoadLocationSuccess(location) );

      },
      (error) => {
        this.store.dispatch(new Msg(MsgType.error, error.message));
      }
    );
  }

  // @Action(LoadLocationSuccess)
  // loadLocationSuccess( ctx: StateContext<LocationStateModel>, action: LoadLocationSuccess) {
  //   console.log("load Success");
  // }


  @Action(LoadAllLocation)
  loadAllLocation( ctx: StateContext<LocationStateModel>) {

    // this.httpClient.get<Location[]>(this.apiBaseUrl + 'companies').subscribe(
    //   (companies) => {
    //     ctx.setState( {
    //       ...ctx.getState(),
    //       loaded: true,
    //       companies: companies
    //     });
    //   }
    // );

    this.httpClient.get<Address[]>(this.apiBaseUrl + 'locations').pipe(
      tap( (locations) => {
        ctx.setState( {
          ...ctx.getState(),
          loaded: true,
          locations: locations
        });
      }
    )).subscribe();

  }

  @Action(DeleteLocation)
  deleteLocation( ctx: StateContext<LocationStateModel>, action: DeleteLocation) {

    this.httpClient.delete(this.apiBaseUrl + 'locations/' + action.id, { responseType: 'text'} ).subscribe(
      (result) => {
        ctx.setState( {
          ...ctx.getState(),
          locations: ctx.getState().locations.filter( (location) => location.id !== action.id )
        });
      },
      (error) => {
        this.store.dispatch(new Msg(MsgType.error, JSON.parse(error.error).message));
      }
    );

  }

  @Action(UpdateLocation)
  updateLocation( ctx: StateContext<LocationStateModel>, action: UpdateLocation ) {

    const location = action.payload;
    this.httpClient.put(
      this.apiBaseUrl + 'locations/' +  location.id,
      location,
      { responseType: 'text' }
    ).subscribe(
      () => {
        // success... time to update my store.
        const state = ctx.getState();
        let idx = 0;
        const clone = state.locations.slice(0);
        clone.some( (c) => {
          if (c.id === +action.payload.id) {
            clone[idx] = action.payload;
            return true;
          }
          idx++;
        });
        ctx.setState( {
          ...ctx.getState(),
          locations: clone
        });
        this.store.dispatch(new Msg(MsgType.success, 'Location is updated.', '/locations'));

      },
      error => {
        this.store.dispatch(new Msg(MsgType.error, JSON.parse(error.error).message));
      }
    );
  }



}
