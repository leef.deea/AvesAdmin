import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { AuthState } from '../login/auth.state';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { Address } from './store/address.model';
import { LoadAllLocation, DeleteLocation } from './store/location.action';
import { LocationStateModel, LocationState } from './store/location.state';


@Component({
  selector: 'app-locations',
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.css']
})
export class LocationsComponent implements OnInit {
  private gridApi;
  rowSelection;
  selectedId: number;
  showColSelection = false;

  @Select(AuthState.isAuthorized) authorized$: Observable<string>;
  @Select( (state) => state.locations.locations ) locations$: Observable<LocationStateModel>;

  columnDefs = [
    {headerName: 'Id', field: 'id' },
    {headerName: 'Name', field: 'name' },
    {headerName: 'Address Line 1', field: 'addressLine1'},
    {headerName: 'Address Line 2', field: 'addressLine2'},
    {headerName: 'City', field: 'city'},
    {headerName: 'State', field: 'state'},
    {headerName: 'Zipcode', field: 'zipcode'},
    {headerName: 'Created By', field: 'createdBy'},
    {headerName: 'Created On', field: 'createdTs'},
  ];


  constructor(private store: Store,
    private router: Router,
    private modal: Modal
  ) { }

  ngOnInit() {
    const locationState = this.store.selectSnapshot( (state) => state.locations );
    if ( !locationState.loaded ) {
      this.store.dispatch( new LoadAllLocation() );
    }
  }

  add() {
    this.router.navigateByUrl('/locations/new');
  }

  onGridReady(params) {
    this.gridApi = params.api;
  }
  onSelectionChanged() {
    const me = this;
    const selectedRows = this.gridApi.getSelectedRows();
    selectedRows.forEach(function(selectedRow, index) {
      me.selectedId = selectedRow.id;
    });
  }
  onDoubleRowDblClk(params) {
    console.log( params );
    this.edit( params.data.id );
  }

  edit( id: number ) {
    if (!id) {
      id = this.selectedId;
    }
    this.router.navigateByUrl('/locations/' + id);
  }

  delete( ) {


    const dialogRef = this.modal.confirm()
    .title(' ')
    .headerClass('modal-header-flee')
    .footerClass('modal-footer-flee')
    .body(`Press OK to delete Company.`)
    .open();

    dialogRef.result.then( result => {
      this.store.dispatch( new DeleteLocation(this.selectedId) );
    } );

  }

  showHideColSelection() {
    this.showColSelection = !this.showColSelection;
    console.log( this.showColSelection );
  }


  // select( id: number ) {
  //   this.selectedId = id;
  // }
  // isSelected( id: number) {
  //   return this.selectedId === id;
  // }


}
