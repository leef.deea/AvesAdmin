import { State, Action, StateContext, Store, Selector, Select } from '@ngxs/store';

import { Login, LoginSuccess, Logout } from './auth.action';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { Msg, MsgType } from '../header/msg.state';
import { environment } from '../../environments/environment.prod';

​
@Injectable()

export class AuthStateModel {
  user: string;
  token: string;
}

@State<AuthStateModel>({
  name: 'auth',
  defaults: {
    user: null,
    token: null
  }
})

export class AuthState {

  apiBaseUrl = environment.apiBaseUrl;
  constructor( private httpClient: HttpClient, private store: Store, private toastr: ToastrService ) {}

  @Selector() static isAuthorized( state ) {
    return state.token !== null;
  }

  @Selector() static getToken( state ): string {
    return state.token;
  }

  @Action(Login)
  login(ctx: StateContext<AuthStateModel>, action: Login ) {

    this.httpClient.post( this.apiBaseUrl + 'auth/login',
      {'usernameOrEmail': action.user, 'password': action.password},
      {observe: 'response'}
    ).subscribe( ( data ) => {
        if ( data.status === 200 ) {
          const state = ctx.getState();
          ctx.setState({
            ...state,
            user: action.user
          });
          this.store.dispatch( new LoginSuccess( 'Bearer ' + (<any>data.body).accessToken ));
        }
    },
    error => {
      console.log( 'Error Login data ');
      this.store.dispatch( new Msg(MsgType.error, 'Invalid login creditential.') );
    }

   );
  }

  @Action(LoginSuccess)
  loginSuccess( ctx: StateContext<AuthStateModel>, action: LoginSuccess) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      ...action
    });
    this.store.dispatch(new Msg(MsgType.success, 'User Logged In', '$back$'));
  }

  @Action(Logout)
  logout( ctx: StateContext<AuthStateModel>, action: Logout) {
    const state = ctx.getState();
    ctx.setState({
      user: null,
      token: null
    });
    this.store.dispatch(new Msg(MsgType.success, 'User Logged Out'));
  }


}
