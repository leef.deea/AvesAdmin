import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TrackerService } from '../tracker.service';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-sprint',
  templateUrl: './sprint.component.html',
  styleUrls: ['./sprint.component.css']
})
export class SprintComponent implements OnInit {

  sprintEditForm: FormGroup;

  constructor(private project: TrackerService) { }

  ngOnInit() {
    this.initForm();
    console.log( this.project );
  }

  initForm() {
    this.sprintEditForm = new FormGroup({
      sprint: new FormControl(this.project.sprint, [
        Validators.required,
        Validators.minLength(4)
      ]),
      lead: new FormControl(this.project.lead, [
        Validators.required,
        Validators.minLength(5)
      ])
    });
    this.sprintEditForm.valueChanges.pipe(debounceTime(500)).subscribe( ( values ) => {
      console.log( values );
      this.project.update( values );
      console.log(this.project);
    });
  }
}
