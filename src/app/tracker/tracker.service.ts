export class TrackerService {


  name: string;
  poc: string;
  startDt: Date;
  sprint: string;
  lead: string;
  gold: string;
  amp: string;

  constructor() {
  }

  update( object: any ) {

    for ( const key of Object.keys(object) ) {
      this[key] = object[key];
    }

  }
}
