import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TrackerService } from './tracker.service';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-tracker',
  templateUrl: './tracker.component.html',
  styleUrls: ['./tracker.component.css']
})
export class TrackerComponent implements OnInit {

  trackerEditForm: FormGroup;


  activeTab = 'sprint';
  constructor(private project: TrackerService) { }

  ngOnInit() {

    this.initForm();

  }


  initForm() {
    this.trackerEditForm = new FormGroup({
      name: new FormControl(this.project.name, [
        Validators.required,
        Validators.minLength(4)
      ]),
      poc: new FormControl(this.project.poc, [
        Validators.required,
        Validators.minLength(5)
      ]),
      start: new FormControl(this.project.startDt)
    });
    this.trackerEditForm.valueChanges.pipe(debounceTime(500)).subscribe( ( values ) => {
      console.log( values );
      this.project.update( values );
      console.log(this.project);
  });



  }

  tabChange( tab: string ) {
    console.log( tab );
    this.activeTab = tab;
  }
  onSubmit() {
    console.log('Time to save....');
    console.log( this.project );
  }

}
