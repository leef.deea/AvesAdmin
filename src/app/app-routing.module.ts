import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

import { AppComponent } from './app.component';
import { LocationsComponent } from './locations/locations.component';
import { CompaniesComponent } from './companies/companies.component';
import { LoginComponent } from './login/login.component';
import { CompanyEditComponent } from './companies/company-edit/company-edit.component';
import { LocationEditComponent } from './locations/location-edit/location-edit.component';
import { CanDeactivateGuard } from './can-deactivate-guard';
import { TrackerComponent } from './tracker/tracker.component';

const appRoutes: Routes = [

  { path: '', component: AppComponent},
  { path: 'locations', component: LocationsComponent },
  { path: 'locations/:id', component: LocationEditComponent, canDeactivate: [CanDeactivateGuard] },
  { path: 'companies', component: CompaniesComponent},
  { path: 'companies/:id', component: CompanyEditComponent, canDeactivate: [CanDeactivateGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'tracker', component: TrackerComponent}


];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
